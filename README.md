# Dice.rs

A diceware library written in Rust, with optional AWS Lambda support.

## Deploying to Lambda

The [instructions on the aws-lambda-rust-runtime repo](https://github.com/awslabs/aws-lambda-rust-runtime) are useful to get up and running. To build `dicers` for Lambda:

```sh
rustup target add x86_64-unknown-linux-musl
# For macOS users
brew install filosottile/musl-cross/musl-cross
# link the installed cross-compiler to a name that `rustc` expects
ln -s /usr/local/bin/x86_64-linux-musl-gcc / usr/local/bin/musl-gcc
cargo build --release --target x86_64-unknown-linux-musl --features "lambda"
```