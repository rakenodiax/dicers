use lazy_static::lazy_static;
use rand::prelude::*;
use std::collections::HashSet;
use std::iter::{Extend, FromIterator};

#[cfg(feature = "lambda")]
mod api;
#[cfg(feature = "lambda")]
pub use crate::api::handler;

lazy_static! {
    static ref DICTIONARY: Dictionary = {
        let dictionary_contents = include_str!("../resources/dictionary.txt");
        Dictionary::read_str(&dictionary_contents)
    };
}

#[derive(Clone, Debug, PartialEq, Default)]
pub struct Dictionary {
    words: HashSet<String>,
}

impl Dictionary {
    pub fn read_str(input: &str) -> Dictionary {
        // `String.lines` implements `Iterator`, so we can use it directly with `FromIterator`
        Dictionary::from_iter(input.lines())
    }
}

impl<S> FromIterator<S> for Dictionary
where
    S: ToString,
{
    fn from_iter<I: IntoIterator<Item = S>>(iter: I) -> Dictionary {
        let words = HashSet::from_iter(iter.into_iter().map(|s| s.to_string()));

        Dictionary { words }
    }
}

impl<S> Extend<S> for Dictionary
where
    S: ToString,
{
    fn extend<I: IntoIterator<Item = S>>(&mut self, iter: I) {
        self.words.extend(iter.into_iter().map(|s| s.to_string()));
    }
}

impl Dictionary {
    pub fn iter(&self) -> DictionaryIterator {
        DictionaryIterator::new(&self.words)
    }
}

pub struct DictionaryIterator<'a> {
    words: &'a HashSet<String>,
    rng: ThreadRng,
}

impl<'a> DictionaryIterator<'a> {
    fn new(words: &'a HashSet<String>) -> DictionaryIterator<'a> {
        let rng = thread_rng();

        DictionaryIterator { words, rng }
    }
}

impl<'a> Iterator for DictionaryIterator<'a> {
    type Item = &'a str;

    fn next(&mut self) -> Option<Self::Item> {
        let word_count = self.words.len();
        let index = self.rng.gen_range(0, word_count);

        self.words.iter().nth(index).map(|s| s.as_str())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn dictionary_implements_from_iterator() {
        let seed = || vec!["foo".to_string(), "bar".to_string()].into_iter();

        let dictionary = Dictionary::from_iter(seed());

        assert_eq!(dictionary.words, HashSet::from_iter(seed()));
    }

    #[test]
    fn dictionary_implements_extend() {
        let addition = || vec!["foo".to_string(), "bar".to_string()].into_iter();

        let mut dictionary = Dictionary::default();

        dictionary.extend(addition());

        assert_eq!(
            dictionary.words,
            HashSet::from_iter(addition().map(|s| s.to_string()))
        );
    }

    #[test]
    fn dictionary_can_be_iterated_over() {
        let word = "foo";

        let dictionary = Dictionary::from_iter(vec![word].into_iter());

        let generated = dictionary.iter().next();

        assert_eq!(generated, Some(word));
    }
}
