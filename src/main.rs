#[cfg(feature = "lambda")]
use dicers::handler;
#[cfg(feature = "lambda")]
use lambda_runtime::lambda;

#[cfg(feature = "lambda")]
fn main() -> Result<(), Box<dyn std::error::Error>> {
    lambda!(handler);

    Ok(())
}

#[cfg(not(feature = "lambda"))]
fn main() {}
