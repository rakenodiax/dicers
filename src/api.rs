use crate::DICTIONARY;
use lambda_runtime::{error::HandlerError, Context};
use serde_derive::{Deserialize, Serialize};

#[derive(Copy, Clone, Debug, Deserialize)]
pub struct GenerateEvent {
    word_count: u8,
    separator: Option<char>,
}

#[derive(Debug, Serialize)]
pub struct GenerateResponse {
    phrase: String,
}

pub fn handler(event: GenerateEvent, _ctx: Context) -> Result<GenerateResponse, HandlerError> {
    match event {
        GenerateEvent {
            word_count,
            separator: Some(separator),
        } => {
            let words: Vec<&str> = DICTIONARY.iter().take(word_count as usize).collect();
            let phrase = words.as_slice().join(&separator.to_string());
            Ok(GenerateResponse { phrase })
        }

        GenerateEvent {
            word_count,
            separator: None,
        } => {
            // Iterators of type `&str` can be joined into one `String` with `collect`
            let phrase: String = DICTIONARY.iter().take(word_count as usize).collect();
            Ok(GenerateResponse { phrase })
        }
    }
}
